package nn.android.advanced.todolist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText mEditText;
    ListView mListView;
    Button mBtnAddUpdate;
    String[] tasks;
    DataHelper dataHelper;
    String oldTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditText =(EditText)findViewById(R.id.editText);
        mListView=(ListView)findViewById(R.id.listView);
        mBtnAddUpdate=(Button) findViewById(R.id.btnAdd);
        dataHelper =new DataHelper(this);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv =(TextView) view;
                oldTask=tv.getText().toString();
                mEditText.setText(oldTask);
                mBtnAddUpdate.setText("Update");
            }
        });
    }

    @Override
    protected  void onStart(){
        super.onStart();
        tasks=dataHelper.getTasks();
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,tasks);
        mListView.setAdapter(adapter);
        mEditText.setText("");
        mBtnAddUpdate.setText("Add");
    }

    public void addUpdate(View v){
        if(mBtnAddUpdate.getText().toString().equals("Add")) {
            String task = mEditText.getText().toString();
            dataHelper.addTask(task);
        }else{
            String task = mEditText.getText().toString();
            dataHelper.update(task,oldTask);
        }
        onStart();
    }
    public void clear(View v){
        dataHelper.clear();
        onStart();
    }
}

