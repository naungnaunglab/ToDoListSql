package nn.android.advanced.todolist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kzs on 5/19/2017.
 */

public class DataHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME="MyTodo2";
    private static final int DATABASE_VERSION = 1;
    public  static final String TABLE_NAME="todo";
    public  static final String CREATE_QUERY="CREATE table "+TABLE_NAME+" (task Text)";

    public DataHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
    public void addTask(String task){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO "+TABLE_NAME+" (task) VALUES ('"+task+"')");
        db.close();
    }
    public String[] getTasks(){
        SQLiteDatabase db =this.getReadableDatabase();
        List<String> tasks =new ArrayList<String>();
        Cursor c = db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        while(c.moveToNext()){
            String task = c.getString(0);
            tasks.add(task);
        }
        c.close();
        db.close();
        return tasks.toArray(new String[0]);
    }
    public void clear(){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_NAME);
        db.close();
    }
    public void update(String task,String oldTask){
        SQLiteDatabase db =this.getWritableDatabase();
        db.execSQL("UPDATE "+TABLE_NAME+" SET task='"+task+"' WHERE task='"+oldTask+"'");
        db.close();

    }
}

